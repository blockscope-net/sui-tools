# SUI Fullnode As Validator Backup Guide

## TLDR

This guide gathers the full process of setting up a fullnode server (with Systemd), and switching to a validator server for backup purposes. This guide contains:

- Setting up a fullnode server from scratch
- Setting both configs: fullnode and validator
- Installing latest snapshot from AWS
- Failover process to switch from main validator to backup validator
- Restore the backup validator back to fullnode server

This guide is based on `Sui official docs` and Chainflow `Validator Recovery Options and Notes` document, please read these docs beforehand to get a better understanding of the whole process and other options.

- [Sui docs - fullnode](https://docs.sui.io/testnet/build/fullnode)
- [Sui docs (nre) - systemd](https://github.com/MystenLabs/sui/blob/main/nre/systemd/README.md)
- [Sui docs - restoring from snapshots](https://docs.sui.io/build/snapshot#restoring-from-snapshots)
- [Chainflow doc - Validator Recovery Options and Notes](https://hackmd.io/@Chainflow/ryew6rsz2)

Any additions are welcome, feel free to create a PR.

# Install fullnode for validator backup

## Create folders and user

```bash
sudo useradd sui

sudo mkdir -p /opt/sui/bin
sudo mkdir -p /opt/sui/config
sudo mkdir -p /opt/sui/db
sudo mkdir -p /opt/sui/key-pairs
sudo chown -R sui:sui /opt/sui
```

## Install dependencies

```bash
sudo apt-get update && sudo apt-get install -y --no-install-recommends tzdata libprotobuf-dev ca-certificates build-essential libssl-dev libclang-dev pkg-config openssl protobuf-compiler git clang cmake
```

## Install Rust

```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

## Build Sui node from source

```bash
SUI_SHA=whatever_latest_version_commit_hash_is

cd ~
git clone https://github.com/MystenLabs/sui.git
cd sui
git checkout $SUI_SHA
cargo build --release --bin sui
cargo build --release --bin sui-node
cargo build --release --bin sui-tool

cp target/release/sui* /opt/sui/bin
```

_Alternatively you can download the binary directly_

```bash
SUI_SHA=whatever_latest_version_commit_hash_is
sudo wget -O /opt/sui/bin/sui-node https://releases.sui.io/${SUI_SHA}/sui-node
```

## Create config files and services

### Download genesis

```bash
curl -fLJO https://github.com/MystenLabs/sui-genesis/raw/main/mainnet/genesis.blob
mv genesis.blob /opt/sui/config
```

_Check urls for other networks https://docs.sui.io/testnet/build/fullnode_

### Download full node and validator config templates

```bash
cp ~/sui/crates/sui-config/data/fullnode-template.yaml /opt/sui/config/fullnode.yaml
wget -O /opt/sui/config/validator.yaml https://raw.githubusercontent.com/MystenLabs/sui/main/nre/config/validator.yaml
```

### Edit fullnode.yaml to add peers, fix routes, ...

```bash
vim /opt/sui/config/fullnode.yaml
```

_Check peers for other networks https://docs.sui.io/testnet/build/fullnode_

```yaml
db-path: "/opt/sui/db/authorities_db"

genesis:
  genesis-file-location: "/opt/sui/config/genesis.blob"

p2p-config:
  seed-peers:
    - address: /dns/icn-00.mainnet.sui.io/udp/8084
      peer-id: 303f1f35afc9a6f82f8d21724f44e1245f4d8eca0806713a07c525dadda95a66
    - address: /dns/icn-01.mainnet.sui.io/udp/8084
      peer-id: cb7ce193cf7a41e9cc2f99e65dd1487b6314a57c74be42cc8c9225b203301812
    - address: /dns/mel-00.mainnet.sui.io/udp/8084
      peer-id: d32b55bdf1737ec415df8c88b3bf91e194b59ee3127e3f38ea46fd88ba2e7849
    - address: /dns/mel-01.mainnet.sui.io/udp/8084
      peer-id: bbf3be337fc16614a1953da83db729abfdc40596e197f36fe408574f7c9b780e
    - address: /dns/ewr-00.mainnet.sui.io/udp/8084
      peer-id: c7bf6cb93ca8fdda655c47ebb85ace28e6931464564332bf63e27e90199c50ee
    - address: /dns/ewr-01.mainnet.sui.io/udp/8084
      peer-id: 3227f8a05f0faa1a197c075d31135a366a1c6f3d4872cb8af66c14dea3e0eb66
    - address: /dns/sjc-00.mainnet.sui.io/udp/8084
      peer-id: 6f0b25087cd6b2fd2e4329bcf308ac95a37c49277dd7286b72470c124809db5b
    - address: /dns/sjc-01.mainnet.sui.io/udp/8084
      peer-id: af1d5d8468b3612ac2b6ff3ca91e99a71390dbe5b83dea9f6ae2da708d689227
    - address: /dns/lhr-00.mainnet.sui.io/udp/8084
      peer-id: c619a5e0f8f36eac45118c1f8bda28f0f508e2839042781f1d4a9818043f732c
    - address: /dns/lhr-01.mainnet.sui.io/udp/8084
      peer-id: 53dcedf250f73b1ec83250614498947db00d17c0181020fcdb7b6db12afbc175
```

_Probably you have to delete the key-pairs definitions too_

### Edit validator.yaml to fix hostname, add metrics, ...

```bash
vim /opt/sui/config/validator.yaml
```

```yaml
p2p-config:
  external-address: /dns/$HOSTNAME/udp/8084

metrics:
  push-url: https://metrics-proxy.mainnet.sui.io:8443/publish/metrics
```

### Grant ownership to all files created

```bash
sudo chown -R sui:sui /opt/sui
```

### Create service file

```bash
sudo wget -O /etc/systemd/system/sui-node.service https://raw.githubusercontent.com/MystenLabs/sui/main/nre/systemd/sui-node.service
```

_Alternatively you can copy/paste it into the file_

```bash
sudo vim /etc/systemd/system/sui-node.service
```

Set the config file for fullnode.yaml instead of validator.yaml

```bash
[Unit]
Description=Sui Node

[Service]
User=sui
WorkingDirectory=/opt/sui/
Environment=RUST_LOG=info,sui_core=debug,narwhal=debug,narwhal-primary::helper=info,jsonrpsee=error
ExecStart=/opt/sui/bin/sui-node --config-path /opt/sui/config/fullnode.yaml
Restart=always

[Install]
WantedBy=multi-user.target
```

### Enable and start the service

```bash
sudo systemctl daemon-reload
sudo systemctl enable sui-node
sudo systemctl start sui-node
sudo systemctl status sui-node

sudo journalctl -u sui-node -fo cat
```

### Monitor it works

```bash
curl 127.0.0.1:9184/metrics 2>/dev/null | grep -E "^last_executed_checkpoint|^highest_synced_checkpoint|^last_committed_round|^current_round|^highest_received_round|^certificates_created|^uptime"

curl http://localhost:9000 --request POST --header 'Content-Type: application/json' --data-raw '{ "jsonrpc":"2.0", "method":"rpc.discover","id":1}'

sudo systemctl stop sui-node
```

## Download snapshot

### NOTE: please refer to official docs for up to date info on dowloading formal snapshots https://docs.sui.io/guides/operator/snapshots#restoring-using-formal-snapshots

```bash
/opt/sui/bin/sui-tool download-formal-snapshot --latest --genesis /opt/sui/config/genesis.blob --network mainnet --path /opt/sui/db/ --num-parallel-downloads 50 --no-sign-request

chown -R sui:sui /opt/sui
```

### Restart and monitor it works

```bash
sudo systemctl start sui-node

curl 127.0.0.1:9184/metrics 2>/dev/null | grep -E "^last_executed_checkpoint|^highest_synced_checkpoint|^last_committed_round|^current_round|^highest_received_round|^certificates_created|^uptime"

curl http://localhost:9000 --request POST --header 'Content-Type: application/json' --data-raw '{ "jsonrpc":"2.0", "method":"rpc.discover","id":1}'
```

## Setup ufw

```bash
sudo ufw allow 8080/tcp
sudo ufw allow 8081/tcp
sudo ufw allow 8081/udp
sudo ufw allow 8082/udp
sudo ufw allow 8083/tcp
sudo ufw allow 8084/udp
sudo ufw allow 8443/tcp
sudo ufw allow 9184/tcp

sudo ufw show added
sudo ufw enable
sudo ufw status verbose
```

# Failover process

_**All operations are for new node, unless specified is OLD NODE**_

## Copy key files

```bash
cp *.key /opt/sui/key-pairs
sudo chown -R sui:sui /opt/sui/
```

## Modify service file to point to validator.yaml

```bash
sudo systemctl stop sui-node
vim /etc/systemd/system/sui-node.service
```

```bash
...
ExecStart=/opt/sui/bin/sui-node --config-path /opt/sui/config/validator.yaml
...
```

```bash
sudo systemctl daemon-reload
```

## Stop node (OLD NODE)

```bash
sudo systemctl stop sui-node
sudo systemctl disable sui-node
```

## Update DNS from old node IP to new node IP

## Restart and monitor it works

```bash
sudo systemctl start sui-node

curl 127.0.0.1:9184/metrics 2>/dev/null | grep -E "^last_executed_checkpoint|^highest_synced_checkpoint|^last_committed_round|^current_round|^highest_received_round|^certificates_created|^uptime"
```

_Wait until last_committed_round catches up with highest_received_round{source="other"} and begins to increase certificates_created regularly. If you start the backup node just after epoch switch it may take about 10 minutes to recreate the consensus_db and begin creating certificates again._

# Restore server

Once main server is back online, you should restore your backup to fullnode again by doing the following steps.

## Stop the node and delete validator db and keys

```bash
sudo systemctl stop sui-node

rm -Rf /opt/sui/db/consensus_db
rm -Rf /opt/sui/key-pairs/*
```

## Modify service file to point to fullnode.yaml

```bash
sudo vim /etc/systemd/system/sui-node.service
```

```bash
...
ExecStart=/opt/sui/bin/sui-node --config-path /opt/sui/config/fullnode.yaml
...
```

## Restart as fullnode

```bash
sudo systemctl daemon-reload
sudo systemctl start sui-node
```
