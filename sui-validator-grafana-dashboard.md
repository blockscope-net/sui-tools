### Grafana Dashboard

This dashboard aims to provide a series of useful charts and data to monitor the health of your testnet and mainnet validator nodes. The dashboard has a select for selecting different server instances, so if you include several servers in your Prometheus config and the corresponding variables in the Grafana dashboard you will have access to inspecting all your instances in the same dashboard.

![Dashboard screenshot 1](image-1.png)

![Dashboard screenshot 2](image-2.png)

_Prometheus Config_

```yaml
- job_name: "sui-metrics"
  static_configs:
    - targets: ["1.1.1.1:9184"]
      labels:
        alias: sui-testnet-metrics
        instance: "Sui Testnet"
    - targets: ["2.2.2.2:9184"]
      labels:
        alias: sui-mainnet-metrics
        instance: "Sui Mainnet"
```

_Grafana Config_

You need the following variables to be able to access the instances you declare in your prometheus through the Host selector on top of the Grafana dashboard.

![Config vars](image.png)

Once declared you should see your instances in the Host selector.

![Dashboard selectors](image-3.png)
